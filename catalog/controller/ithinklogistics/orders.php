<?php  
error_reporting(0);
header('Content-type: application/json');
class ControllerIthinklogisticsOrders extends Controller
{
  	public function index() 
  	{
  		$host 				= DB_HOSTNAME;
		$user 				= DB_USERNAME;
		$password 			= DB_PASSWORD;
		$database 			= DB_DATABASE;
		$db_mysqli 			= new mysqli($host, $user, $password, $database);
		mysqli_set_charset($db_mysqli,"utf8");

		$db_prefix 			= DB_PREFIX;
		$order_table 		= $db_prefix."order";
		$product_table 		= $db_prefix."order_product";

		$db_access_token 	= 'fbd224692d23265cf4d1cee79a8dccd5';
		$db_secret_key	 	= '3b0834c23e1b5ad1cab88971e4c39505';

		$access_token 		= $_POST['access_token'];
		$secret_key 		= $_POST['secret_key'];
		if(isset($_POST['order_update']))
		{
			$order_update 		= $_POST['order_update'];
		}
		else
		{
			$order_update 		= 0;
		}
		
		
		
		if($db_access_token == $access_token && $db_secret_key = $secret_key)
		{
			if($order_update == 1)
			{
				$order_id 			= $_POST['order_id'];
				$order_status_id 	= $_POST['order_status_id'];
				$comment 			= $_POST['comment'];
				$notify 			= $_POST['notify'];
				$override 			= $_POST['override'];

				$this->load->model('checkout/order');
				$order_info = $this->model_checkout_order->getOrder($order_id);
				if ($order_info) 
				{
					$this->model_checkout_order->addOrderHistory($order_id, $order_status_id, $comment, $notify, $override);
					$return = array();
				    $return["html_message"] = 'Order updated successfully.';
			        $return["status"] = "success";
			        echo json_encode($return);
			        exit();
				} 
				else 
				{
					$return = array();
				    $return["html_message"] = 'Error updating order information.';
			        $return["status"] = "error";
			        echo json_encode($return);
			        exit();
				}
				exit();
			}
			else
			{
				$total_count 		= $_POST['total_count'];
				$last_updated 		= $_POST['last_updated'];
				$page 				= $_POST['page'];
				$order_id 			= $_POST['order_id'];
				$per_page_orders	= 50;
				if($total_count == 1) // Return total number of order count modified after provided date
				{
					if($last_updated != '')
					{
						$get_order_query = "SELECT count(*) as total_orders from $order_table where date_modified >= '$last_updated'";	
					}
					else
					{	
						$get_order_query = "SELECT count(*) as total_orders from $order_table";
					}

					$total_orders = 0;
				    $result_get_order_query = mysqli_query($db_mysqli, $get_order_query); 
					while ($row_get_order_query = mysqli_fetch_assoc($result_get_order_query))
				    {
				        $total_orders = $row_get_order_query['total_orders'];
				    }
				    
				    $return = array();
				    $return["html_message"] = 'Success';
			        $return["status"] = "success";
			        $return["total_orders"] = $total_orders;
			        echo json_encode($return);
			        exit();
				}
				else // Return all the orders modified after provided date
				{
					if($order_id != '')
					{
						$get_order_query = "SELECT * from $order_table where order_id = '$order_id'";	
					}
					else if($last_updated != '')
					{
						$limit = ' Limit 0,50';
						if($page != '' && $page > 0)
						{
							$sql_offset = ($page-1)*$per_page_orders;
							$limit = " Limit ".$sql_offset.",".$per_page_orders;
						}
						$get_order_query = "SELECT * from $order_table where date_modified >= '$last_updated' $limit";	
					}
					else
					{
						$limit = ' Limit 0,50';
						if($page != '' && $page > 0)
						{
							$sql_offset = ($page-1)*$per_page_orders;
							$limit = " Limit ".$sql_offset.",".$per_page_orders;
						}
						$get_order_query = "SELECT * from $order_table $limit";
					}
				    
				    $all_order_data_array = array();
				    $all_order_id = '';
				    $all_order_id_array = array();
				    $result_get_order_query = mysqli_query($db_mysqli, $get_order_query); 
				    while ($row_get_order_query = mysqli_fetch_assoc($result_get_order_query))
				    {
				        $all_order_data_array[$row_get_order_query['order_id']] = $row_get_order_query;
				        $order_id = $row_get_order_query['order_id'];
				        if(!in_array($order_id,$all_order_id_array))
				        {
				        	$all_order_id_array[] = $order_id;
				        	$all_order_id .= $order_id.",";
				        }
				    }
				    if(strlen($all_order_id) > 1)
				    {
				    	$all_order_id = substr($all_order_id,0,-1);

				    	$all_order_product_data_array = array();
				    	$get_order_product_query = "SELECT * from $product_table where order_id IN ($all_order_id)";
				    	$result_get_order_product_query = mysqli_query($db_mysqli, $get_order_product_query); 
					    while ($row_get_order_product = mysqli_fetch_assoc($result_get_order_product_query))
					    {
					        $all_order_product_data_array[$row_get_order_product['order_id']][] = $row_get_order_product;
					    }

					    foreach($all_order_data_array as $all_order_data)
					    {
					    	$all_order_data_array[$all_order_data['order_id']]['products'] = $all_order_product_data_array[$all_order_data['order_id']];
					    }
				    }
				    
				    //print_r($all_order_data_array);

				    $return = array();
				    $return["html_message"] = 'Success';
			        $return["status"] = "success";
			        $return["all_order_data_array"] = $all_order_data_array;
			        echo json_encode($return);
			        exit();
				}
			}
			
			
			
		}
		else
		{
			$return = array();
		    $return["html_message"] = 'Access key & secret key does not match.';
	        $return["status"] = "error";
	        echo json_encode($return);
	        exit();
		}
	    
  	}


}
?>