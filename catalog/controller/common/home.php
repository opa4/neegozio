<?php
class ControllerCommonHome extends Controller {
	public function index() {
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        //$this->load->model('extension/module');
        $data['banner_info'] = $this->model_catalog_category->getBanners();
        $data['banner_mid'] = $this->model_catalog_category->getBannersMid();
        $data['banner_end'] = $this->model_catalog_category->getBannersEnd();
        $data['feedbacks'] = $this->model_catalog_category->getFeedbacks();
        $data['featured_product'] =$this->model_catalog_category->getModule(28);
        $data['new_product'] =$this->model_catalog_category->getModule(33);
        $data['best_seller_product'] =$this->model_catalog_category->getModule(35);
        $data['new_arrival_product'] =$this->model_catalog_category->getModule(34);
        $data['special_deal_product'] =$this->model_catalog_category->getModule(37);
        $data['special_offer_product'] =$this->model_catalog_category->getModule(38);
        $data['hot_deal_product'] =$this->model_catalog_category->getModule(36);
        $data['trending_product'] =$this->model_catalog_category->getModule(39);
        //$featured_prd = implode(",",$data['featured_product']['product']);
        $featured_prd = $data['featured_product']['product'];
//        $filter_data = array(
//            'prd_id' => $featured_prd,
//        );
        $new_prd = $data['new_product']['product'];
        $best_seller_prd = $data['best_seller_product']['product'];
        $new_arrival_prd = $data['new_arrival_product']['product'];
        $special_deal_prd = $data['special_deal_product']['product'];
        $special_offer_prd = $data['special_offer_product']['product'];
        $hot_deal_prd = $data['hot_deal_product']['product'];
        $trending_prd = $data['trending_product']['product'];
//        $filter_data = array(
//            'prd_id' => $new_prd,
//        );
        //$data['product_featured'] = $this->model_catalog_category->getProductsSlider($filter_data);
        foreach ($featured_prd as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $product_info['image'];
                } else {
                    $image = 'placeholder.png';
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $sp = preg_replace("/[^0-9.]/", "", $product_info['special']);
                    $pr = preg_replace("/[^0-9.]/", "", $product_info['price']);

                    $discount = 100 - round(($sp/$pr)*100);
                } else {
                    $special = false;
                    $discount = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['product_featured'][] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'discount'     => $discount,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
                shuffle($data['product_featured']);
            }
        }
        foreach ($new_prd as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $product_info['image'];
                } else {
                    $image = 'placeholder.png';
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $sp = preg_replace("/[^0-9.]/", "", $product_info['special']);
                    $pr = preg_replace("/[^0-9.]/", "", $product_info['price']);

                    $discount = 100 - round(($sp/$pr)*100);
                } else {
                    $special = false;
                    $discount = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['product_new'][] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'discount'     => $discount,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
                shuffle($data['product_new']);
            }
        }
        foreach ($best_seller_prd as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $product_info['image'];
                } else {
                    $image = 'placeholder.png';
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $sp = preg_replace("/[^0-9.]/", "", $product_info['special']);
                    $pr = preg_replace("/[^0-9.]/", "", $product_info['price']);

                    $discount = 100 - round(($sp/$pr)*100);
                } else {
                    $special = false;
                    $discount = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['product_best_seller'][] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'discount'     => $discount,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
                shuffle($data['product_best_seller']);
            }
        }
        foreach ($new_arrival_prd as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $product_info['image'];
                } else {
                    $image = 'placeholder.png';
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $sp = preg_replace("/[^0-9.]/", "", $product_info['special']);
                    $pr = preg_replace("/[^0-9.]/", "", $product_info['price']);

                    $discount = 100 - round(($sp/$pr)*100);
                } else {
                    $special = false;
                    $discount = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['product_new_arrival'][] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'discount'     => $discount,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
                shuffle($data['product_new_arrival']);
            }
        }
        foreach ($special_deal_prd as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $product_info['image'];
                } else {
                    $image = 'placeholder.png';
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $sp = preg_replace("/[^0-9.]/", "", $product_info['special']);
                    $pr = preg_replace("/[^0-9.]/", "", $product_info['price']);

                    $discount = 100 - round(($sp/$pr)*100);
                } else {
                    $special = false;
                    $discount = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['product_special_deal'][] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'discount'     => $discount,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
                shuffle($data['product_special_deal']);
            }
        }
        foreach ($special_offer_prd as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $product_info['image'];
                } else {
                    $image = 'placeholder.png';
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $sp = preg_replace("/[^0-9.]/", "", $product_info['special']);
                    $pr = preg_replace("/[^0-9.]/", "", $product_info['price']);

                    $discount = 100 - round(($sp/$pr)*100);
                } else {
                    $special = false;
                    $discount = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['product_special_offer'][] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'discount'     => $discount,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
                shuffle($data['product_special_offer']);
            }
        }
        foreach ($hot_deal_prd as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $product_info['image'];
                } else {
                    $image = 'placeholder.png';
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $sp = preg_replace("/[^0-9.]/", "", $product_info['special']);
                    $pr = preg_replace("/[^0-9.]/", "", $product_info['price']);

                    $discount = 100 - round(($sp/$pr)*100);
                } else {
                    $special = false;
                    $discount = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['product_hot_deal'][] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'discount'     => $discount,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
                shuffle($data['product_hot_deal']);
            }
        }
        foreach ($trending_prd as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $product_info['image'];
                } else {
                    $image = 'placeholder.png';
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $sp = preg_replace("/[^0-9.]/", "", $product_info['special']);
                    $pr = preg_replace("/[^0-9.]/", "", $product_info['price']);

                    $discount = 100 - round(($sp/$pr)*100);
                } else {
                    $special = false;
                    $discount = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['product_trending'][] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'discount'     => $discount,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                );
                shuffle($data['product_trending']);
            }
        }
        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategoriesForHome();
        foreach ($categories as $category) {
            $data['categories'][] = array(
                'name'     => $category['name'],
                'column'   => $category['column'] ? $category['column'] : 1,
                'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
            );
        }
        
        $product_tags = $this->model_catalog_category->getCategoriesForHomeTags();
        foreach ($product_tags as $product_tag) {
            $data['product_tags'][] = array(
                'name'     => $product_tag['name'],
                'column'   => $product_tag['column'] ? $category['column'] : 1,
                'href'     => $this->url->link('product/category', 'path=' . $product_tag['category_id'])
            );
        }
        
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
