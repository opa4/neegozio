<?php
class ControllerAccountSeller extends Controller {
    private $error = array();

    public function index() {
        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('account/account', '', true));
        }
//		$seller_id = $this->request->post('seller');
//		echo $seller_id;exit;
        $this->load->language('account/register');

        $this->load->language('purpletree_multivendor/sellerstore');

        $this->load->language('account/ptsregister');


        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->load->model('account/customer');

        $this->load->model('localisation/country');



        $data['countries'] = $this->model_localisation_country->getCountries();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            //echo '<pre>';print_r($this->request->post);exit;
            $first = substr($this->request->post['firstname'], 0, 1);
            $last = substr($this->request->post['lastname'], 0, 1);
            $con = $first.$last;
            $this->request->post['seller_alpha_code'] =  'V-'.$con.substr(str_shuffle("123456"), -5).mt_rand(1000,99999);
            $this->request->post['store_bank_details'] = 'Bank Name : '.$this->request->post['store_bank'].'    '.'Branch Name : '.$this->request->post['branch_name'].'   '.'Account Number : '.$this->request->post['account_num'].'   '.'Account Holders Name : '.$this->request->post['account_name'].'   '.'IFSC Code : '.$this->request->post['ifsc_code'].'   '.'Accounty Type : '.$this->request->post['account_type'];

//            echo '<pre>';print_r($this->request->post);
//            exit;
            $customer_id = $this->model_account_customer->addCustomer($this->request->post);

            if ($this->config->get('module_purpletree_multivendor_become_seller')) {
                if($this->request->post['become_seller']=="1"){
                    $store_name = trim($this->request->post['seller_storename']);
                    $this->load->model('extension/purpletree_multivendor/vendor');
                    $file = '';
                    $seller_id = $this->model_extension_purpletree_multivendor_vendor->addSeller($customer_id,$store_name ,$file);
                }
            }



            // Clear any previous login attempts for unregistered accounts.
            $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

            $this->customer->login($this->request->post['email'], $this->request->post['password']);

            unset($this->session->data['guest']);

            $this->response->redirect($this->url->link('account/success'));
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_register'),
            'href' => $this->url->link('account/seller', '', true)
        );
        $data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));


        $data['module_purpletree_multivendor_status'] = $this->config->get('module_purpletree_multivendor_status');
        $data['module_purpletree_multivendor_become_seller'] = $this->config->get('module_purpletree_multivendor_become_seller');
        if ($this->config->get('module_purpletree_multivendor_become_seller')) {
            $data['text_seller'] = $this->language->get('text_seller');
            $data['text_seller_heading'] = $this->language->get('text_seller_heading');
            $data['text_store_name'] = $this->language->get('text_store_name');
        }
        if (isset($this->request->post['become_seller'])) {
            $data['become_seller'] = $this->request->post['become_seller'];
        } else {
            $data['become_seller'] = '';
        }
        if (isset($this->request->post['seller_storename'])) {
            $data['seller_storename'] = $this->request->post['seller_storename'];
        } else {
            $data['seller_storename'] = '';
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['firstname'])) {
            $data['error_firstname'] = $this->error['firstname'];
        } else {
            $data['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $data['error_lastname'] = $this->error['lastname'];
        } else {
            $data['error_lastname'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }
        if (isset($this->error['otp'])) {
            $data['error_otp'] = $this->error['otp'];
        } else {
            $data['error_otp'] = '';
        }

        if (isset($this->error['custom_field'])) {
            $data['error_custom_field'] = $this->error['custom_field'];
        } else {
            $data['error_custom_field'] = array();
        }

        if (isset($this->error['password'])) {
            $data['error_password'] = $this->error['password'];
        } else {
            $data['error_password'] = '';
        }


        if (isset($this->error['seller_store'])) {
            $data['error_sellerstore'] = $this->error['seller_store'];
        } else {
            $data['error_sellerstore'] = '';
        }
        if (isset($this->error['warning1'])) {
            $data['error_warning1'] = $this->error['warning1'];
        } else {
            $data['error_warning1'] = '';
        }

        if (isset($this->error['confirm'])) {
            $data['error_confirm'] = $this->error['confirm'];
        } else {
            $data['error_confirm'] = '';
        }
        if (isset($this->error['store_bank_details'])) {
            $data['error_storebankdetail'] = $this->error['store_bank_details'];
        } else {
            $data['error_storebankdetail'] = '';
        }

        if (isset($this->error['store_bank'])) {
            $data['error_store_bank'] = $this->error['store_bank'];
        } else {
            $data['error_store_bank'] = '';
        }

        if (isset($this->error['branch_name'])) {
            $data['error_branch_name'] = $this->error['branch_name'];
        } else {
            $data['error_branch_name'] = '';
        }

        if (isset($this->error['account_num'])) {
            $data['error_account_num'] = $this->error['account_num'];
        } else {
            $data['error_account_num'] = '';
        }
        if (isset($this->error['account_name'])) {
            $data['error_account_name'] = $this->error['account_name'];
        } else {
            $data['error_account_name'] = '';
        }

        if (isset($this->error['ifsc_code'])) {
            $data['error_ifsc_code'] = $this->error['ifsc_code'];
        } else {
            $data['error_ifsc_code'] = '';
        }



        if (isset($this->error['store_address'])) {
            $data['error_store_address'] = $this->error['store_address'];
        } else {
            $data['error_store_address'] = '';
        }

        if (isset($this->error['store_country'])) {
            $data['error_storecountry'] = $this->error['store_country'];
        } else {
            $data['error_storecountry'] = '';
        }


        if (isset($this->error['store_city'])) {
            $data['error_storecity'] = $this->error['store_city'];
        } else {
            $data['error_storecity'] = '';
        }

        if (isset($this->request->post['store_zipcode'])) {
            $data['store_zipcode'] = $this->request->post['store_zipcode'];
        } else {
            $data['store_zipcode'] = '';
        }

        if (isset($this->request->post['store_state'])) {
            $data['store_state'] = $this->request->post['store_state'];
        }  else {
            $data['store_state'] = '';

        }

        if (isset($this->error['store_gst'])) {
            $data['error_storegst'] = $this->error['store_gst'];
        } else {
            $data['error_storegst'] = '';
        }
        if (isset($this->error['brand_name'])) {
            $data['error_brandname'] = $this->error['brand_name'];
        } else {
            $data['error_brandname'] = '';
        }

        $data['action'] = $this->url->link('account/seller', '', true);

        $data['customer_groups'] = array();

        if (is_array($this->config->get('config_customer_group_display'))) {
            $this->load->model('account/customer_group');

            $customer_groups = $this->model_account_customer_group->getCustomerGroups();

            foreach ($customer_groups as $customer_group) {
                if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
                    $data['customer_groups'][] = $customer_group;
                }
            }
        }

        if (isset($this->request->post['customer_group_id'])) {
            $data['customer_group_id'] = $this->request->post['customer_group_id'];
        } else {
            $data['customer_group_id'] = $this->config->get('config_customer_group_id');
        }

        if (isset($this->request->post['firstname'])) {
            $data['firstname'] = $this->request->post['firstname'];
        } else {
            $data['firstname'] = '';
        }

        if (isset($this->request->post['lastname'])) {
            $data['lastname'] = $this->request->post['lastname'];
        } else {
            $data['lastname'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } else {
            $data['telephone'] = '';
        }
        if (isset($this->request->post['otp'])) {
            $data['otp'] = $this->request->post['otp'];
        } else {
            $data['otp'] = '';
        }
        if (isset($this->request->post['otp_val'])) {
            $data['otp_val'] = $this->request->post['otp_val'];
        } else {
            $data['otp_val'] = '';
        }
        if (isset($this->request->post['seller_storename'])) {
            $data['seller_storename'] = $this->request->post['seller_storename'];
        } else {
            $data['seller_storename'] = '';
        }
        if (isset($this->request->post['store_bank_details'])) {
            $data['store_bank_details'] = $this->request->post['store_bank_details'];
        } else {
            $data['store_bank_details'] = '';
        }


        if (isset($this->request->post['store_bank'])) {
            $data['store_bank'] = $this->request->post['store_bank'];
        } else {
            $data['store_bank'] = '';
        }
        if (isset($this->request->post['branch_name'])) {
            $data['branch_name'] = $this->request->post['branch_name'];
        } else {
            $data['branch_name'] = '';
        }
        if (isset($this->request->post['account_num'])) {
            $data['account_num'] = $this->request->post['account_num'];
        } else {
            $data['account_num'] = '';
        }
        if (isset($this->request->post['account_name'])) {
            $data['account_name'] = $this->request->post['account_name'];
        } else {
            $data['account_name'] = '';
        }
        if (isset($this->request->post['ifsc_code'])) {
            $data['ifsc_code'] = $this->request->post['ifsc_code'];
        } else {
            $data['ifsc_code'] = '';
        }
        if (isset($this->request->post['account_type'])) {
            $data['account_type'] = $this->request->post['account_type'];
        } else {
            $data['account_type'] = '';
        }


        if (isset($this->request->post['store_address'])) {
            $data['store_address'] = $this->request->post['store_address'];
        } else {
            $data['store_address'] = '';
        }

        if (isset($this->request->post['store_city'])) {
            $data['store_city'] = $this->request->post['store_city'];
        } else {
            $data['store_city'] = '';
        }
        if (isset($this->request->post['store_country'])) {
            $data['store_country'] = $this->request->post['store_country'];
        } else {
            $data['store_country'] = '';
        }

        if (isset($this->error['store_city'])) {
            $data['error_storecity'] = $this->error['store_city'];
        } else {
            $data['error_storecity'] = '';
        }

        if (isset($this->error['error_storezone'])) {
            $data['error_storezone'] = $this->error['error_storezone'];
        } else {
            $data['error_storezone'] = '';
        }
        if (isset($this->error['store_zipcode'])) {
            $data['error_storezipcode'] = $this->error['store_zipcode'];
        } else {
            $data['error_storezipcode'] = '';
        }
        if (isset($this->request->post['store_gst'])) {
            $data['store_gst'] = $this->request->post['store_gst'];
        } else {
            $data['store_gst'] = '';
        }
        if (isset($this->request->post['brand_name'])) {
            $data['brand_name'] = $this->request->post['brand_name'];
        } else {
            $data['brand_name'] = '';
        }

        // Custom Fields
        $data['custom_fields'] = array();

        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields();

        foreach ($custom_fields as $custom_field) {
            if ($custom_field['location'] == 'account') {
                $data['custom_fields'][] = $custom_field;
            }
        }

        if (isset($this->request->post['custom_field']['account'])) {
            $data['register_custom_field'] = $this->request->post['custom_field']['account'];
        } else {
            $data['register_custom_field'] = array();
        }

        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } else {
            $data['password'] = '';
        }

        if (isset($this->request->post['confirm'])) {
            $data['confirm'] = $this->request->post['confirm'];
        } else {
            $data['confirm'] = '';
        }

        if (isset($this->request->post['newsletter'])) {
            $data['newsletter'] = $this->request->post['newsletter'];
        } else {
            $data['newsletter'] = '';
        }

        // Captcha
        if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
            $data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
        } else {
            $data['captcha'] = '';
        }

        if ($this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info) {
                $data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), true), $information_info['title'], $information_info['title']);
            } else {
                $data['text_agree'] = '';
            }
        } else {
            $data['text_agree'] = '';
        }

        if (isset($this->request->post['agree'])) {
            $data['agree'] = $this->request->post['agree'];
        } else {
            $data['agree'] = false;
        }
        if (isset($this->request->post['seller_aggrement'])) {
            $data['seller_aggrement'] = $this->request->post['seller_aggrement'];
        } else {
            $data['seller_aggrement'] = '';
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/seller', $data));
    }

    public function otp(){
        $num = '91'.$_POST['id'];
        $otp = substr(number_format(time() * rand(),0,'',''),0,6);
//        SMS GATEWAY TESTING

        // Authorisation details.
        $username = "nirjhargoswami@neegozio.com";
        $hash = "27bb2e4d50d9765db32e686f09faf548c2582f77a7e69aa9fe37424906a0c420";

        // Config variables. Consult http://api.textlocal.in/docs for more info.
        $test = "0";

        // Data for text message. This is the text message data.
        $sender = "NEEGOZ"; // This is who the message appears to be from.
        $numbers = $num; // A single number or a comma-seperated list of numbers
        $message = "Thank you for registering with Neegozio.com your verification code is $otp";

        // 612 chars or less
        // A single number or a comma-seperated list of numbers

        //$message = urlencode($message);
        $message = rawurlencode($message);
        $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
        $ch = curl_init('http://api.textlocal.in/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); // This is the result from the API
        curl_close($ch);

        echo $otp;

    }

    private function validate() {

        $this->load->model('extension/purpletree_multivendor/vendor');

        if($this->config->get('module_purpletree_multivendor_become_seller')) {
            /*if($this->request->post['become_seller'] == 1){
                $data['become_seller'] = $this->request->post['become_seller'];
                if((utf8_strlen($this->request->post['seller_storename']) < 5) || (utf8_strlen(trim($this->request->post['seller_storename'])) > 50)) {
                    $this->error['seller_store'] = $this->language->get('error_storename');
                }

                $store_info1 = $this->model_extension_purpletree_multivendor_vendor->getStoreNameByStoreName($this->request->post['seller_storename']);
                //echo"<pre>";print_r($this->request->post['seller_storename']);die;
                if ($store_info1 && (strtoupper(trim($this->request->post['seller_storename']))==strtoupper($store_info1['store_name']))) {
                    $this->error['seller_store'] = $this->language->get('error_exist_storename');
                    $this->error['warning'] = $this->language->get('error_exist_storename');
                }

            }*/
        }

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
            $this->error['warning'] = $this->language->get('error_exists');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        if (($this->request->post['otp'] != $this->request->post['otp_val'])) {
            //echo 'Hi';exit;
            //echo '<pre>';print_r($this->request->post['otp']);exit;
            $this->error['otp'] = $this->language->get('error_otp');
        }

        // Customer Group
        if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $this->request->post['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        // Custom field validation
        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

        foreach ($custom_fields as $custom_field) {
            if ($custom_field['location'] == 'account') {
                if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                } elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                }
            }
        }

        if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
            $this->error['password'] = $this->language->get('error_password');
        }

        if ($this->request->post['confirm'] != $this->request->post['password']) {
            $this->error['confirm'] = $this->language->get('error_confirm');
        }
        if($this->request->post['become_seller'] == 1) {
//            if ((utf8_strlen(trim($this->request->post['store_bank_details'])) <= 0) && isset($this->request->post['store_bank_details'])) {
//                $this->error['store_bank_details'] = $this->language->get('error_storebankdetail');
//            }

            if ((utf8_strlen(trim($this->request->post['branch_name'])) <= 0) && isset($this->request->post['branch_name'])) {
                $this->error['branch_name'] = $this->language->get('error_branch_name');
            }

//            if ((utf8_strlen(trim($this->request->post['store_bank_details'])) <= 0) && isset($this->request->post['store_bank_details'])) {
//                $this->error['store_bank_details'] = $this->language->get('error_storebankdetail');
//            }

            if ((utf8_strlen(trim($this->request->post['account_num'])) <= 0) && isset($this->request->post['account_num'])) {
                $this->error['account_num'] = $this->language->get('error_account_num');
            }

            if ((utf8_strlen(trim($this->request->post['ifsc_code'])) <= 0) && isset($this->request->post['ifsc_code'])) {
                $this->error['ifsc_code'] = $this->language->get('error_ifsc_code');
            }
            if ((utf8_strlen(trim($this->request->post['account_name'])) <= 0) && isset($this->request->post['account_name'])) {
                $this->error['account_name'] = $this->language->get('error_account_name');
            }


        }
        if($this->request->post['become_seller'] == 1) {
            if ((utf8_strlen(trim($this->request->post['store_address'])) <= 0) && isset($this->request->post['store_address'])) {
                $this->error['store_address'] = $this->language->get('error_store_address');
            }
        }
        if($this->request->post['become_seller'] == 1) {
            if ((utf8_strlen(trim($this->request->post['store_city'])) < 3) || (utf8_strlen(trim($this->request->post['store_city'])) > 50)) {
                $this->error['store_city'] = $this->language->get('error_storecity');
            }
            if (empty($this->request->post['store_state'])) {
                $this->error['error_storezone'] = $this->language->get('error_storezone');
            }
            if(trim($this->request->post['store_zipcode']) >= 1){
                if ((utf8_strlen(trim($this->request->post['store_zipcode'])) < 3) || (utf8_strlen(trim($this->request->post['store_zipcode'])) > 12)) {
                    $this->error['store_zipcode'] = $this->language->get('error_storepostcode');
                }

            }
        }
        if($this->request->post['become_seller'] == 1) {
            if (empty($this->request->post['store_country'])) {
                $this->error['store_country'] = $this->language->get('error_storecountry');
            }
        }

        if ($this->request->post['become_seller'] == 1){
            $regex = "/^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/";
            $gstin = trim($this->request->post['store_gst']);
            if ((utf8_strlen(trim($this->request->post['store_gst'])) < 3) || (preg_match($regex, $gstin)==0)) {
                $this->error['store_gst'] = $this->language->get('error_storegst');
            }
        }

        // Captcha
        /*if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
            $captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

            if ($captcha) {
                $this->error['captcha'] = $captcha;
            }
        }*/

        // Agree to terms
        if($this->request->post['become_seller'] == 1){
            if($this->request->post['seller_aggrement'] != 1 ){
                $this->error['warning'] = $this->language->get('error_seller_aggrement');
            }

        }
        if($this->request->post['become_seller'] == 0){
            if ($this->config->get('config_account_id')) {
                $this->load->model('catalog/information');

                $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

                if ($information_info && !isset($this->request->post['agree'])) {
                    $this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
                }
            }
        }


        return !$this->error;
    }

    public function customfield() {
        $json = array();

        $this->load->model('account/custom_field');

        // Customer Group
        if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $this->request->get['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

        foreach ($custom_fields as $custom_field) {
            $json[] = array(
                'custom_field_id' => $custom_field['custom_field_id'],
                'required'        => $custom_field['required']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}