<?php 
class ControllerExtensionAccountPurpletreeMultivendorDashboardicons extends Controller {
	private $error = array();
	
	public function index(){
			
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('extension/account/purpletree_multivendor/dashboard', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}


	
		$this->load->language('purpletree_multivendor/dashboard');
		$this->load->model('extension/purpletree_multivendor/dashboard');
		$data['seller_orders'] = array();

        if (isset($this->request->post['seller_price'])) {
            $data['seller_price'] = $this->request->post['seller_price'];
            $ratecalculator_info = $this->model_extension_purpletree_multivendor_dashboard->rateCalculator();
            $expense = $data['seller_price']+$ratecalculator_info['shipping_fee'];
            $fixed_fee = $ratecalculator_info['pack_print_fee']+$ratecalculator_info['marketplace_fee'];
            $commision_fee = ($data['seller_price']*$ratecalculator_info['comission_fee'])/100;
            $collection_fee = ($expense*$ratecalculator_info['collection_fee'])/100;
            $return_cost = ($expense*$ratecalculator_info['return_cost'])/100;
            $selling_price = $expense+$fixed_fee+$commision_fee+$collection_fee+$return_cost;
            $gst = ($selling_price*5)/100;
            $result_1 = $gst+$selling_price;
            $result = round($result_1);
            $data['result'] = $result;

        }
		

		if (isset($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];

			unset($this->session->data['error_warning']);
		} else {
			$data['error_warning'] = '';
		}
		$url ='';
		$data['breadcrumbs'] = array();
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home','',true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title1'),
			'href' => $this->url->link('extension/account/purpletree_multivendor/dashboardicons', $url, true)
		);
		$data['isSeller'] = $this->customer->isSeller();
		$store_id = (isset($data['isSeller']['id'])?$data['isSeller']['id']:'');
		$this->load->model('localisation/order_status');
		$this->document->setTitle($this->language->get('heading_title1'));
		$data['heading_title']=$this->language->get('heading_title1');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer'); 
		$data['header'] = $this->load->controller('common/header');

		
				$data['sellerprofile'] = $this->url->link('extension/account/edit', '', true);
				$data['downloadsitems'] = $this->url->link('extension/account/purpletree_multivendor/downloads', '', true);
				$data['sellerstore'] = $this->url->link('extension/account/purpletree_multivendor/sellerstore', '', true);
				$data['sellerproduct'] = $this->url->link('extension/account/purpletree_multivendor/sellerproduct', '', true);
				$orderstatus = 0;
				if(null !== $this->config->get('module_purpletree_multivendor_commission_status')) {
					$orderstatus = $this->config->get('module_purpletree_multivendor_commission_status');
				}
				$data['sellerorder'] = $this->url->link('extension/account/purpletree_multivendor/sellerorder', 'filter_order_status='.$orderstatus.'&filter_admin_order_status='.$orderstatus.'', true);
				$data['sellercommission'] = $this->url->link('extension/account/purpletree_multivendor/sellercommission', '', true);
				$data['removeseller'] = $this->url->link('extension/account/purpletree_multivendor/sellerstore/removeseller', '', true);
				$data['becomeseller'] = $this->url->link('extension/account/purpletree_multivendor/sellerstore/becomeseller', '', true);
				$data['sellerview'] = $this->url->link('extension/account/purpletree_multivendor/sellerstore/storeview&seller_store_id='.$store_id, '', true);
				$data['sellerreview'] = $this->url->link('extension/account/purpletree_multivendor/sellerstore/sellerreview', '', true);
				$data['sellerenquiry'] = $this->url->link('extension/account/purpletree_multivendor/sellercontact/sellercontactlist', '', true);
				$data['dashboardicons'] = $this->url->link('extension/account/purpletree_multivendor/dashboardicons', '', true);
				$data['dashboard'] = $this->url->link('extension/account/purpletree_multivendor/dashboard', '', true);
											
	
		$this->response->setOutput($this->load->view('account/purpletree_multivendor/dashboardicons', $data));
	}	

}
