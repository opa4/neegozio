<?php
class ModelCatalogRatecalculator extends Model {
	public function addRatecalculator($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "ratecalculator SET shipping_fee = '" . $this->db->escape($data['shipping_fee']) . "', pack_print_fee = '" . $this->db->escape($data['pack_print_fee']) . "', marketplace_fee = '" . $this->db->escape($data['marketplace_fee']) . "', comission_fee = '" . $this->db->escape($data['comission_fee']) . "', collection_fee = '" . $this->db->escape($data['collection_fee']) . "', return_cost = '" . $this->db->escape($data['return_cost']) . "', gst = '" . $this->db->escape($data['gst']) . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

		$ratecalculator_id = $this->db->getLastId();

		//$this->cache->delete('product');

		return $ratecalculator_id;
	}

	public function editRatecalculator($ratecalculator_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "ratecalculator SET shipping_fee = '" . $this->db->escape($data['shipping_fee']) . "', pack_print_fee = '" . $this->db->escape($data['pack_print_fee']) . "', marketplace_fee = '" . $this->db->escape($data['marketplace_fee']) . "', comission_fee = '" . $this->db->escape($data['comission_fee']) . "', collection_fee = '" . $this->db->escape($data['collection_fee']) . "', return_cost = '" . $this->db->escape($data['return_cost']) . "', gst = '" . $this->db->escape($data['gst']) . "', status = '" . (int)$data['status'] . "', date_added = '" . $this->db->escape($data['date_added']) . "', date_modified = NOW() WHERE ratecalculator_id = '" . (int)$ratecalculator_id . "'");

		$this->cache->delete('product');
	}

	public function deleteRatecalculator($ratecalculator_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ratecalculator WHERE ratecalculator_id = '" . (int)$ratecalculator_id . "'");

		//$this->cache->delete('product');
	}

	public function getRatecalculator($ratecalculator_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "ratecalculator r WHERE r.ratecalculator_id = '" . (int)$ratecalculator_id . "'");

		return $query->row;
	}

	public function getRatecalculators($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "ratecalculator r";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$sort_data = array(
			'r.shipping_fee',
			'r.pack_print_fee',
			'r.marketplace_fee',
			'r.comission_fee'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY r.date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalRatecalculators($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "ratecalculator";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalRatecalculatorsAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "ratecalculator WHERE status = '0'");

		return $query->row['total'];
	}
}