<?php
// Heading
$_['heading_title']     = 'Rate Calculator';

// Text
$_['text_success']      = 'Success: You have modified Rate Calculator settings!';
$_['text_list']         = 'Review List';
$_['text_add']          = 'Add Review';
$_['text_edit']         = 'Edit Review';
$_['text_filter']       = 'Filter';

// Column
$_['column_product']    = 'Product';
$_['column_author']     = 'Author';
$_['column_rating']     = 'Rating';
$_['column_status']     = 'Status';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

$_['column_shipping_fee']     = 'Shipping Fee';
$_['column_pack_print_fee']     = 'Package & Printing Fee';
$_['column_marketplace_fee']     = 'Marketplace Fee';

// Entry
$_['entry_product']     = 'Product';
$_['entry_author']      = 'Author';
$_['entry_rating']      = 'Rating';
$_['entry_status']      = 'Status';
$_['entry_text']        = 'Text';
$_['entry_feedback']    = 'Feedback';

$_['entry_shipping_fee']    = 'Shipping Fee';
$_['entry_pack_print_fee']    = 'Packing & Printing Fee';
$_['entry_marketplace_fee']    = 'Market Place Fee';
$_['entry_comission_fee']    = 'Comission Fee % ';
$_['entry_collection_fee']    = 'Collection Fee % ';
$_['entry_return_cost']    = 'Return Cost/Order % ';
$_['entry_gst']    = 'GST %';



$_['entry_date_added']  = 'Date Added';

// Help
$_['help_product']      = '(Autocomplete)';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify reviews!';
$_['error_product']     = 'Product required!';
$_['error_author']      = 'Author must be between 3 and 64 characters!';
$_['error_text']        = 'Review Text must be at least 1 character!';

$_['error_gst']      = 'GST required!';
$_['error_return_cost']      = 'Return Cost required!';
$_['error_collection_fee']      = 'Collection Fee required!';
$_['error_comission_fee']      = 'Commision Fee required!';
$_['error_marketplace_fee']      = 'Marketplace Fee required!';
$_['error_pack_print_fee']      = 'Packing & Printing Fee required!';
$_['error_shipping_fee']      = 'Shipping Fee required!';