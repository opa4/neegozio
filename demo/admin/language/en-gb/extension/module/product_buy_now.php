<?php
// Heading
$_['heading_title']	   = 'Product Page Buynow';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']	   = 'Success: You have modified Product Page Buynow!';
$_['text_edit']		   = 'Edit Product Page Buynow';

// Entry
$_['entry_status']	   = 'Product page buy now button Status';
$_['entry_listing_status']	   = 'Product Listing buy now button Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Product Page Buynow!';
