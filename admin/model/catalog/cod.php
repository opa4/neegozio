<?php
class ModelCatalogCod extends Model {
	public function addCod($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "cod SET cod_area = '" . $this->db->escape($data['cod_area']) . "' ,prepaid_area = '" . $this->db->escape($data['prepaid_area']) . "' ,status = '" . (int)$data['status'] . "', date_added = NOW()");

		$cod_id = $this->db->getLastId();

		//$this->cache->delete('product');

		return $cod_id;
	}

	public function editCod($cod_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "cod SET cod_area = '" . $this->db->escape($data['cod_area']) . "',prepaid_area = '" . $this->db->escape($data['prepaid_area']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE cod_id = '" . (int)$cod_id . "'");

	}

	public function deleteCod($cod_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cod WHERE cod_id = '" . (int)$cod_id . "'");

		$this->cache->delete('product');
	}

	public function getCod($cod_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cod r WHERE r.cod_id = '" . (int)$cod_id . "'");

		return $query->row;
	}

	public function getCods($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "cod r ";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_cod_area'])) {
			$sql .= " AND r.cod_area LIKE '" . $this->db->escape($data['filter_cod_area']) . "%'";
		}
		if (!empty($data['filter_prepaid_area'])) {
			$sql .= " AND r.prepaid_area LIKE '" . $this->db->escape($data['filter_prepaid_area']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$sort_data = array(
			'r.cod_area',
			'r.prepaid_area',
			'r.status',
			'r.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY r.date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalCods($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cod r ";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalCodsAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cod WHERE status = '0'");

		return $query->row['total'];
	}
}