<?php
class ModelCatalogSareetype extends Model {
	public function addSareetype($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_desc SET description = '" . $this->db->escape($data['description']) . "', flag = '" . $this->db->escape($data['flag'])  . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

		$pd_id = $this->db->getLastId();

		$this->cache->delete('product');

		return $pd_id;
	}

	public function editSareetype($pd_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "product_desc SET description = '" . $this->db->escape($data['description']) . "', flag = '" . $this->db->escape($data['flag']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE pd_id = '" . (int)$pd_id . "'");

		$this->cache->delete('product');
	}

	public function deleteSareetype($pd_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_desc WHERE pd_id = '" . (int)$pd_id . "'");

		$this->cache->delete('product');
	}

	public function getSareetype($pd_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product_desc r WHERE r.pd_id = '" . (int)$pd_id . "'");

		return $query->row;
	}

	public function getSareetypes($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product_desc r where r.pd_id IS NOT NULL";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}
		if (!empty($data['flag'])) {
			$sql .= " AND flag LIKE '" . $this->db->escape($data['flag']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$sort_data = array(
			'pd.name',
			'r.author',
			'r.rating',
			'r.status',
			'r.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY r.date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSareetypes($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_desc r  WHERE r.pd_id IS NOT NULL";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}
        if (!empty($data['flag'])) {
            $sql .= " AND flag LIKE '" . $this->db->escape($data['flag']) . "%'";
        }

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalSareetypesAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_desc WHERE status = '0'");

		return $query->row['total'];
	}
}