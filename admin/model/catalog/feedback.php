<?php
class ModelCatalogFeedback extends Model {
	public function addFeedback($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "feedback SET author = '" . $this->db->escape($data['author']) . "',image = '" . $this->db->escape($data['image']) . "', feedback = '" . $this->db->escape(strip_tags($data['feedback'])) . "', company_name = '" . $this->db->escape($data['company_name']) . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

		$feedback_id = $this->db->getLastId();

		$this->cache->delete('product');

		return $feedback_id;
	}

	public function editFeedback($feedback_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "feedback SET author = '" . $this->db->escape($data['author']) . "',image = '" . $this->db->escape($data['image']) . "',feedback = '" . $this->db->escape(strip_tags($data['feedback'])) . "',company_name = '" . $this->db->escape($data['company_name']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE feedback_id = '" . (int)$feedback_id . "'");

	}

	public function deleteFeedback($feedback_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "feedback WHERE feedback_id = '" . (int)$feedback_id . "'");

		$this->cache->delete('product');
	}

	public function getFeedback($feedback_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "feedback r WHERE r.feedback_id = '" . (int)$feedback_id . "'");

		return $query->row;
	}

	public function getFeedbacks($data = array()) {
		$sql = "SELECT r.*FROM " . DB_PREFIX . "feedback r Where r.feedback_id IS NOT NULL";



		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}
		if (!empty($data['filter_company_name'])) {
			$sql .= " AND r.company_name LIKE '" . $this->db->escape($data['filter_company_name']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$sort_data = array(
			'r.author',
			'r.company_name',
			'r.status',
			'r.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY r.date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalFeedbacks($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "feedback r Where r.feedback_id IS NOT NULL";



		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}
        if (!empty($data['filter_company_name'])) {
            $sql .= " AND r.company_name LIKE '" . $this->db->escape($data['filter_company_name']) . "%'";
        }

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalFeedbacksAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "feedback WHERE status = '0'");

		return $query->row['total'];
	}
}