<?php
class ControllerCatalogRatecalculator extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/ratecalculator');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/ratecalculator');

		$this->getList();
        $this->load->model('user/user');
	}

	public function add() {

		$this->load->language('catalog/ratecalculator');



		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/ratecalculator');

        $this->load->model('user/user');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_ratecalculator->addRatecalculator($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_author'])) {
				$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/ratecalculator');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/ratecalculator');
        $this->load->model('user/user');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_ratecalculator->editRatecalculator($this->request->get['ratecalculator_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_author'])) {
				$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/ratecalculator');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/ratecalculator');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $ratecalculator_id) {
				$this->model_catalog_ratecalculator->deleteRatecalculator($ratecalculator_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_author'])) {
				$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_product'])) {
			$filter_product = $this->request->get['filter_product'];
		} else {
			$filter_product = '';
		}

		if (isset($this->request->get['filter_author'])) {
			$filter_author = $this->request->get['filter_author'];
		} else {
			$filter_author = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '';
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = '';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'r.date_added';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/ratecalculator/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/ratecalculator/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['ratecalculators'] = array();

		$filter_data = array(
			'filter_product'    => $filter_product,
			'filter_author'     => $filter_author,
			'filter_status'     => $filter_status,
			'filter_date_added' => $filter_date_added,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             => $this->config->get('config_limit_admin')
		);

		$ratecalculator_total = $this->model_catalog_ratecalculator->getTotalRatecalculators($filter_data);

		$results = $this->model_catalog_ratecalculator->getRatecalculators($filter_data);

		foreach ($results as $result) {
			$data['ratecalculators'][] = array(
				'ratecalculator_id'  => $result['ratecalculator_id'],
				'shipping_fee'       => $result['shipping_fee'],
				'pack_print_fee'     => $result['pack_print_fee'],
				'marketplace_fee'     => $result['marketplace_fee'],
				'comission_fee'     => $result['comission_fee'],
				'collection_fee'     => $result['collection_fee'],
                'return_cost'     => $result['return_cost'],
                'gst'     => $result['gst'],
				'status'     => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'edit'       => $this->url->link('catalog/ratecalculator/edit', 'user_token=' . $this->session->data['user_token'] . '&ratecalculator_id=' . $result['ratecalculator_id'] . $url, true)
			);
		}

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$data['sort_shipping_fee'] = $this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . '&sort=r.shipping_fee' . $url, true);
		$data['sort_pack_print_fee'] = $this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . '&sort=r.pack_print_fee' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . '&sort=r.status' . $url, true);
		$data['sort_date_added'] = $this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . '&sort=r.date_added' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $ratecalculator_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($ratecalculator_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($ratecalculator_total - $this->config->get('config_limit_admin'))) ? $ratecalculator_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $ratecalculator_total, ceil($ratecalculator_total / $this->config->get('config_limit_admin')));

		$data['filter_product'] = $filter_product;
		$data['filter_author'] = $filter_author;
		$data['filter_status'] = $filter_status;
		$data['filter_date_added'] = $filter_date_added;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/ratecalculator_list', $data));
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['ratecalculator_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['pack_print_fee'])) {
			$data['error_pack_print_fee'] = $this->error['pack_print_fee'];
		} else {
			$data['error_pack_print_fee'] = '';
		}
		if (isset($this->error['marketplace_fee'])) {
			$data['error_marketplace_fee'] = $this->error['marketplace_fee'];
		} else {
			$data['error_marketplace_fee'] = '';
		}

		if (isset($this->error['shipping_fee'])) {
			$data['error_shipping_fee'] = $this->error['shipping_fee'];
		} else {
			$data['error_shipping_fee'] = '';
		}

		if (isset($this->error['text'])) {
			$data['error_text'] = $this->error['text'];
		} else {
			$data['error_text'] = '';
		}
		if (isset($this->error['comission_fee'])) {
			$data['error_comission_fee'] = $this->error['comission_fee'];
		} else {
			$data['error_comission_fee'] = '';
		}

		if (isset($this->error['rating'])) {
			$data['error_rating'] = $this->error['rating'];
		} else {
			$data['error_rating'] = '';
		}
		if (isset($this->error['collection_fee'])) {
			$data['error_collection_fee'] = $this->error['collection_fee'];
		} else {
			$data['error_collection_fee'] = '';
		}
		if (isset($this->error['return_cost'])) {
			$data['error_return_cost'] = $this->error['return_cost'];
		} else {
			$data['error_return_cost'] = '';
		}
		if (isset($this->error['gst'])) {
			$data['error_gst'] = $this->error['gst'];
		} else {
			$data['error_gst'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['ratecalculator_id'])) {
			$data['action'] = $this->url->link('catalog/ratecalculator/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/ratecalculator/edit', 'user_token=' . $this->session->data['user_token'] . '&ratecalculator_id=' . $this->request->get['ratecalculator_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/ratecalculator', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['ratecalculator_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$ratecalculator_info = $this->model_catalog_ratecalculator->getRatecalculator($this->request->get['ratecalculator_id']);
		}

		$data['user_token'] = $this->session->data['user_token'];
        $user_info = $this->model_user_user->getUser($this->user->getId());
        $data['created_by'] = $user_info['username'];
		$this->load->model('catalog/product');

		if (isset($this->request->post['marketplace_fee'])) {
			$data['marketplace_fee'] = $this->request->post['marketplace_fee'];
		} elseif (!empty($ratecalculator_info)) {
			$data['marketplace_fee'] = $ratecalculator_info['marketplace_fee'];
		} else {
			$data['marketplace_fee'] = '';
		}

		if (isset($this->request->post['pack_print_fee'])) {
			$data['pack_print_fee'] = $this->request->post['pack_print_fee'];
		} elseif (!empty($ratecalculator_info)) {
			$data['pack_print_fee'] = $ratecalculator_info['pack_print_fee'];
		} else {
			$data['pack_print_fee'] = '';
		}

		if (isset($this->request->post['shipping_fee'])) {
			$data['shipping_fee'] = $this->request->post['shipping_fee'];
		} elseif (!empty($ratecalculator_info)) {
			$data['shipping_fee'] = $ratecalculator_info['shipping_fee'];
		} else {
			$data['shipping_fee'] = '';
		}

//		if (isset($this->request->post['text'])) {
//			$data['text'] = $this->request->post['text'];
//		} elseif (!empty($ratecalculator_info)) {
//			$data['text'] = $ratecalculator_info['text'];
//		} else {
//			$data['text'] = '';
//		}
		if (isset($this->request->post['comission_fee'])) {
			$data['comission_fee'] = $this->request->post['comission_fee'];
		} elseif (!empty($ratecalculator_info)) {
			$data['comission_fee'] = $ratecalculator_info['comission_fee'];
		} else {
			$data['comission_fee'] = '';
		}
		if (isset($this->request->post['created_by'])) {
			$data['created_by'] = $this->request->post['created_by'];
		} elseif (!empty($ratecalculator_info)) {
			$data['created_by'] = $ratecalculator_info['created_by'];
		} else {
			$data['created_by'] = '';
		}

		if (isset($this->request->post['collection_fee'])) {
			$data['collection_fee'] = $this->request->post['collection_fee'];
		} elseif (!empty($ratecalculator_info)) {
			$data['collection_fee'] = $ratecalculator_info['collection_fee'];
		} else {
			$data['collection_fee'] = '';
		}
		if (isset($this->request->post['return_cost'])) {
			$data['return_cost'] = $this->request->post['return_cost'];
		} elseif (!empty($ratecalculator_info)) {
			$data['return_cost'] = $ratecalculator_info['return_cost'];
		} else {
			$data['return_cost'] = '';
		}
		if (isset($this->request->post['gst'])) {
			$data['gst'] = $this->request->post['gst'];
		} elseif (!empty($ratecalculator_info)) {
			$data['gst'] = $ratecalculator_info['gst'];
		} else {
			$data['gst'] = '';
		}

		if (isset($this->request->post['date_added'])) {
			$data['date_added'] = $this->request->post['date_added'];
		} elseif (!empty($ratecalculator_info)) {
			$data['date_added'] = ($ratecalculator_info['date_added'] != '0000-00-00 00:00' ? $ratecalculator_info['date_added'] : '');
		} else {
			$data['date_added'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($ratecalculator_info)) {
			$data['status'] = $ratecalculator_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/ratecalculator_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/ratecalculator')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['pack_print_fee']) {
			$this->error['pack_print_fee'] = $this->language->get('error_pack_print_fee');
		}
		if (!$this->request->post['marketplace_fee']) {
			$this->error['marketplace_fee'] = $this->language->get('error_marketplace_fee');
		}

		if ((utf8_strlen($this->request->post['shipping_fee']) < 3) || (utf8_strlen($this->request->post['shipping_fee']) > 64)) {
			$this->error['shipping_fee'] = $this->language->get('error_shipping_fee');
		}

//		if (utf8_strlen($this->request->post['text']) < 1) {
//			$this->error['text'] = $this->language->get('error_text');
//		}
		if (utf8_strlen($this->request->post['comission_fee']) < 1) {
			$this->error['comission_fee'] = $this->language->get('error_comission_fee');
		}
        if (utf8_strlen($this->request->post['return_cost']) < 1) {
            $this->error['return_cost'] = $this->language->get('error_return_cost');
        }
        if (utf8_strlen($this->request->post['gst']) < 1) {
            $this->error['gst'] = $this->language->get('error_gst');
        }
        if (utf8_strlen($this->request->post['collection_fee']) < 1) {
            $this->error['collection_fee'] = $this->language->get('error_collection_fee');
        }

//		if (!isset($this->request->post['collection_fee']) || $this->request->post['collection_fee'] < 0 || $this->request->post['collection_fee'] > 5) {
//			$this->error['collection_fee'] = $this->language->get('error_collection_fee');
//		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/ratecalculator')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}