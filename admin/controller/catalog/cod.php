<?php
class ControllerCatalogCod extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/cod');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/cod');

		$this->getList();
        $this->load->model('user/user');
	}

	public function add() {

		$this->load->language('catalog/cod');



		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/cod');

        $this->load->model('user/user');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_cod->addCod($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_cod_area'])) {
				$url .= '&filter_cod_area=' . urlencode(html_entity_decode($this->request->get['filter_cod_area'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_prepaid_area'])) {
				$url .= '&filter_prepaid_area=' . urlencode(html_entity_decode($this->request->get['filter_prepaid_area'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/cod');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/cod');
        $this->load->model('user/user');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_cod->editCod($this->request->get['cod_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_cod_area'])) {
				$url .= '&filter_cod_area=' . urlencode(html_entity_decode($this->request->get['filter_cod_area'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_prepaid_area'])) {
				$url .= '&filter_prepaid_area=' . urlencode(html_entity_decode($this->request->get['filter_prepaid_area'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/cod');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/cod');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $cod_id) {
				$this->model_catalog_cod->deleteCod($cod_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_cod_area'])) {
				$url .= '&filter_cod_area=' . urlencode(html_entity_decode($this->request->get['filter_cod_area'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_prepaid_area'])) {
				$url .= '&filter_prepaid_area=' . urlencode(html_entity_decode($this->request->get['filter_prepaid_area'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {


		if (isset($this->request->get['filter_cod_area'])) {
			$filter_cod_area = $this->request->get['filter_cod_area'];
		} else {
			$filter_cod_area = '';
		}
		if (isset($this->request->get['filter_prepaid_area'])) {
			$filter_prepaid_area = $this->request->get['filter_prepaid_area'];
		} else {
			$filter_prepaid_area = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '';
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = '';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'r.date_added';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';



		if (isset($this->request->get['filter_cod_area'])) {
			$url .= '&filter_cod_area=' . urlencode(html_entity_decode($this->request->get['filter_cod_area'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_prepaid_area'])) {
			$url .= '&filter_prepaid_area=' . urlencode(html_entity_decode($this->request->get['filter_prepaid_area'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/cod/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/cod/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['cods'] = array();

		$filter_data = array(
			'filter_cod_area'     => $filter_cod_area,
			'filter_prepaid_area'     => $filter_prepaid_area,
			'filter_status'     => $filter_status,
			'filter_date_added' => $filter_date_added,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             => $this->config->get('config_limit_admin')
		);

		$cod_total = $this->model_catalog_cod->getTotalCods($filter_data);

		$results = $this->model_catalog_cod->getCods($filter_data);

		foreach ($results as $result) {
			$data['cods'][] = array(
				'cod_id'  => $result['cod_id'],
				'cod_area'     => $result['cod_area'],
				'prepaid_area'     => $result['prepaid_area'],
				'status'     => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'edit'       => $this->url->link('catalog/cod/edit', 'user_token=' . $this->session->data['user_token'] . '&cod_id=' . $result['cod_id'] . $url, true)
			);
		}

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_cod_area'])) {
			$url .= '&filter_cod_area=' . urlencode(html_entity_decode($this->request->get['filter_cod_area'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_prepaid_area'])) {
			$url .= '&filter_prepaid_area=' . urlencode(html_entity_decode($this->request->get['filter_prepaid_area'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$data['sort_cod_area'] = $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . '&sort=r.cod_area' . $url, true);
		$data['sort_prepaid_area'] = $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . '&sort=r.prepaid_area' . $url, true);
		$data['sort_rating'] = $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . '&sort=r.rating' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . '&sort=r.status' . $url, true);
		$data['sort_date_added'] = $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . '&sort=r.date_added' . $url, true);

		$url = '';



		if (isset($this->request->get['filter_cod_area'])) {
			$url .= '&filter_cod_area=' . urlencode(html_entity_decode($this->request->get['filter_cod_area'], ENT_QUOTES, 'UTF-8'));
		}
if (isset($this->request->get['filter_prepaid_area'])) {
			$url .= '&filter_prepaid_area=' . urlencode(html_entity_decode($this->request->get['filter_prepaid_area'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $cod_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($cod_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($cod_total - $this->config->get('config_limit_admin'))) ? $cod_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $cod_total, ceil($cod_total / $this->config->get('config_limit_admin')));


		$data['filter_cod_area'] = $filter_cod_area;
		$data['filter_prepaid_area'] = $filter_prepaid_area;
		$data['filter_status'] = $filter_status;
		$data['filter_date_added'] = $filter_date_added;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/cod_list', $data));
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['cod_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}



		if (isset($this->error['cod_area'])) {
			$data['error_cod_area'] = $this->error['cod_area'];
		} else {
			$data['error_cod_area'] = '';
		}
		if (isset($this->error['prepaid_area'])) {
			$data['error_prepaid_area'] = $this->error['prepaid_area'];
		} else {
			$data['error_prepaid_area'] = '';
		}






		$url = '';



		if (isset($this->request->get['filter_cod_area'])) {
			$url .= '&filter_cod_area=' . urlencode(html_entity_decode($this->request->get['filter_cod_area'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_prepaid_area'])) {
			$url .= '&filter_prepaid_area=' . urlencode(html_entity_decode($this->request->get['filter_prepaid_area'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['cod_id'])) {
			$data['action'] = $this->url->link('catalog/cod/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/cod/edit', 'user_token=' . $this->session->data['user_token'] . '&cod_id=' . $this->request->get['cod_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/cod', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['cod_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$cod_info = $this->model_catalog_cod->getCod($this->request->get['cod_id']);
		}

		$data['user_token'] = $this->session->data['user_token'];
        $user_info = $this->model_user_user->getUser($this->user->getId());
        $data['feedback_by'] = $user_info['username'];
		$this->load->model('catalog/product');





		if (isset($this->request->post['cod_area'])) {
			$data['cod_area'] = $this->request->post['cod_area'];
		} elseif (!empty($cod_info)) {
			$data['cod_area'] = $cod_info['cod_area'];
		} else {
			$data['cod_area'] = '';
		}
		if (isset($this->request->post['prepaid_area'])) {
			$data['prepaid_area'] = $this->request->post['prepaid_area'];
		} elseif (!empty($cod_info)) {
			$data['prepaid_area'] = $cod_info['prepaid_area'];
		} else {
			$data['prepaid_area'] = '';
		}







		if (isset($this->request->post['date_added'])) {
			$data['date_added'] = $this->request->post['date_added'];
		} elseif (!empty($cod_info)) {
			$data['date_added'] = ($cod_info['date_added'] != '0000-00-00 00:00' ? $cod_info['date_added'] : '');
		} else {
			$data['date_added'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($cod_info)) {
			$data['status'] = $cod_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/cod_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/cod')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['cod_area']) < 6)) {
			$this->error['cod_area'] = $this->language->get('error_cod_area');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/cod')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}