<?php
class ControllerExtensionModuleProductBuynow extends Controller {
	private $error = array();

	public function index() {
 		$this->load->language('extension/module/product_buy_now');
		$this->load->model('setting/setting');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('buy_now', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module/product_buy_now', 'user_token=' . $this->session->data['user_token'], true));
		}
	//
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_listing_status'] = $this->language->get('entry_listing_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/module', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/product_buy_now', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/product_buy_now', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['buy_now_status'])) {
			$data['buy_now_status'] = $this->request->post['buy_now_status'];
		} else {
			$data['buy_now_status'] = $this->config->get('buy_now_status');
		}
		
		if (isset($this->request->post['buy_now_color'])) {
			$data['buy_now_color'] = $this->request->post['buy_now_color'];
		} else {
			$data['buy_now_color'] = $this->config->get('buy_now_color');
		}
		
		if (isset($this->request->post['buy_now_width'])) {
			$data['buy_now_width'] = $this->request->post['buy_now_width'];
		} else {
			$data['buy_now_width'] = $this->config->get('buy_now_width');
		}
		
		if (isset($this->request->post['buy_now_height'])) {
			$data['buy_now_height'] = $this->request->post['buy_now_height'];
		} else {
			$data['buy_now_height'] = $this->config->get('buy_now_height');
		}
		if (isset($this->request->post['buy_now_font_size'])) {
			$data['buy_now_font_size'] = $this->request->post['buy_now_font_size'];
		} else {
			$data['buy_now_font_size'] = $this->config->get('buy_now_font_size');
		}
		if (isset($this->request->post['buy_now_text'])) {
			$data['buy_now_text'] = $this->request->post['buy_now_text'];
		} else {
			$data['buy_now_text'] = $this->config->get('buy_now_text');
		}
		
		if (isset($this->request->post['buy_now_text_color'])) {
			$data['buy_now_text_color'] = $this->request->post['buy_now_text_color'];
		} else {
			$data['buy_now_text_color'] = $this->config->get('buy_now_text_color');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/product_buy_now', $data));
	}
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/product_buy_now')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if($this->request->post['buy_now_status'] == 1){
			if($this->request->post['buy_now_color'] == '' || $this->request->post['buy_now_width'] == '' || $this->request->post['buy_now_height'] == '' || $this->request->post['buy_now_text'] == '' || $this->request->post['buy_now_text_color'] == ''){
				$this->error['warning'] = 'Please fill all detail';
			}
		}
		return !$this->error;
	}
}
