<?php
// Heading
$_['heading_title']     = 'Area Codes';

// Text
$_['text_success']      = 'Success: You have modified Area Codes!';
$_['text_list']         = 'COD Area Codes List';
$_['text_add']          = 'Add COD Area Code';
$_['text_edit']         = 'Edit COD Area Code';
$_['text_filter']       = 'Filter';

// Column
$_['column_cod_area']     = 'COD Area Pincodes';
$_['column_prepaid_area']     = 'Prepaid Area Pincodes';
$_['column_rating']     = 'Rating';
$_['column_status']     = 'Status';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry
$_['entry_cod_area']      = 'COD Area Pincodes';
$_['entry_prepaid_area']      = 'Preapid Area Pincodes';
$_['entry_status']      = 'Status';

$_['entry_date_added']  = 'Date Added';

// Help

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Area Codes!';
$_['error_cod_area']      = 'Pincodes must be more thar 6  characters!';
$_['error_text']        = 'Area Codes Text must be at least 1 character!';
$_['error_rating']      = 'Area Codes rating required!';