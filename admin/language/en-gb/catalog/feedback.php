<?php
// Heading
$_['heading_title']     = 'Feedbacks';

// Text
$_['text_success']      = 'Success: You have modified Feedbacks!';
$_['text_list']         = 'Feedback List';
$_['text_add']          = 'Add Feedback';
$_['text_edit']         = 'Edit Feedback';
$_['text_filter']       = 'Filter';

// Column
$_['column_author']     = 'Feedback By';
$_['column_company']     = 'Company Name';
$_['column_status']     = 'Status';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry
$_['entry_image']     = 'Upload Your Image';
$_['entry_author']      = 'Author';
$_['entry_company_name']  = 'Company Name';
$_['entry_status']      = 'Status';
$_['entry_text']        = 'Text';
$_['entry_feedback']    = 'Feedback';

$_['entry_date_added']  = 'Date Added';

// Help
$_['help_product']      = '(Autocomplete)';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify reviews!';
$_['error_product']     = 'Product required!';
$_['error_author']      = 'Author must be between 3 and 64 characters!';
$_['error_text']        = 'Review Text must be at least 1 character!';
$_['error_rating']      = 'Review rating required!';